#include <iostream>
#include <math.h>
int w = 120, h = 30;

char* pixelMap = new char[w*h+1];
float aspect = (float) w / h, pixelAspect = 0.5f;
int main(){
	pixelMap[w*h] = '\0';
	for (int t = 1;;t++)
	{
		for(int i = 0; i < w; i++)
		{
			for (int j = 0; j < h; j++)
			{
				float x = (float) i / w * 2.0f - 1.0f;
				float y = (float) j / h * 2.0f - 1.0f;
				x*=aspect * pixelAspect;
				x+=sinf(t * 0.001f);
				char pixel = ' ';
				if(x*x+y*y < 0.5) pixel = '#';
				pixelMap[i + j * w] = pixel;
			}
		}
		std::cout << pixelMap;
	}
}